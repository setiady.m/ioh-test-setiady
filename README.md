# Ioh Test Setiady



## Getting started

To make it easy for you to get started with IOH Project Test, here's a list of recommended next steps.

## Clone Project

- [ ] Clone Project files
```
git clone https://gitlab.com/setiady.m/ioh-test-setiady.git
git checkout main
```

- [ ] Iniate Project

```
go mod init iohtestsetiady
go mod tidy
go mod vendor
go mod verify

```

## Schema DB

- [ ] Structure Table
```
CREATE DATABASE db_invoice;

USE db_invoice;

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `due_date` date NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` float NOT NULL,
  `xendit_invoice_id` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoice_number` (`invoice_number`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
```

## API Docs Swagger

- [ ] Install and run Swag CMD
```
go install github.com/swaggo/swag/cmd/swag@v1.8.6
swag init
```

## RUN API

- [ ] Run API server
```
go run main.go
```

## Open API Docs Swagger
- After run server, you can open api docs with this url
```
http://localhost:8002/docs/index.html
```


## Collection API
- [ ] Register
```
curl --location 'http://localhost:8002/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "setiady_mulya90",
    "email": "setiady.mulya@aol.com",
    "firstname": "Adys",
    "lastname": "Mulyas",
    "password": "987654321"
}'
```

- [ ] Login
```
curl --location 'http://localhost:8002/login' \
--header 'Content-Type: application/json' \
--data '{
    "username": "setiady_mulya",
    "password": "987654321"
}'
```

- [ ] Create Invoice
```
curl --location 'http://localhost:8002/invoices' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNldGlhZHkubXVseWFAeWFob28uY29tIiwiZXhwIjoxNzE3ODQwNDI1LCJmaXJzdG5hbWUiOiJBZHkiLCJsYXN0bmFtZSI6Ik11bHlhIiwidXNlcm5hbWUiOiJzZXRpYWR5X211bHlhIn0.UtAPwlwdRYDoVEppmy8CftoLE1qT6_WbkCVshFFUfdc' \
--data '{
    "invoice_number": "987654311",
    "due_date": "2020-04-10",
    "item_name": "car",
    "quantity": 1,
    "amount": 100000000
}'
```

- [ ] Update Invoice
```
curl --location --request PUT 'http://localhost:8002/invoices/8' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNldGlhZHkubXVseWFAeWFob28uY29tIiwiZXhwIjoxNzE3ODQwNDI1LCJmaXJzdG5hbWUiOiJBZHkiLCJsYXN0bmFtZSI6Ik11bHlhIiwidXNlcm5hbWUiOiJzZXRpYWR5X211bHlhIn0.UtAPwlwdRYDoVEppmy8CftoLE1qT6_WbkCVshFFUfdc' \
--data '{
    "due_date": "2020-04-10",
    "item_name": "book1",
    "quantity": 1,
    "amount": 10
}'
```

- [ ] List Invoice
```
curl --location 'http://localhost:8002/invoices?page=1&pageSize=10' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNldGlhZHkubXVseWFAeWFob28uY29tIiwiZXhwIjoxNzE3ODQwNDI1LCJmaXJzdG5hbWUiOiJBZHkiLCJsYXN0bmFtZSI6Ik11bHlhIiwidXNlcm5hbWUiOiJzZXRpYWR5X211bHlhIn0.UtAPwlwdRYDoVEppmy8CftoLE1qT6_WbkCVshFFUfdc'
```

- [ ] Show Invoice
```
curl --location 'http://localhost:8002/invoices/7' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNldGlhZHkubXVseWFAeWFob28uY29tIiwiZXhwIjoxNzE3ODQwNDI1LCJmaXJzdG5hbWUiOiJBZHkiLCJsYXN0bmFtZSI6Ik11bHlhIiwidXNlcm5hbWUiOiJzZXRpYWR5X211bHlhIn0.UtAPwlwdRYDoVEppmy8CftoLE1qT6_WbkCVshFFUfdc'
```

- [ ] Delete Invoice
```
curl --location --request DELETE 'http://localhost:8002/invoices/6' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNldGlhZHkubXVseWFAeWFob28uY29tIiwiZXhwIjoxNzE3ODQwNDI1LCJmaXJzdG5hbWUiOiJBZHkiLCJsYXN0bmFtZSI6Ik11bHlhIiwidXNlcm5hbWUiOiJzZXRpYWR5X211bHlhIn0.UtAPwlwdRYDoVEppmy8CftoLE1qT6_WbkCVshFFUfdc'
```

## Unit Test
- [ ] Run unit test
```
go test -v
```