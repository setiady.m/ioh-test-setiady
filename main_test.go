package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"
)

// Mock database connection
var (
	mockDB *sql.DB
)

func TestMain(m *testing.M) {
	// Setup
	err := godotenv.Load()
	if err != nil {
		panic("Error loading .env file")
	}

	// Create mock database connection
	mockDB, _, _ = sqlmock.New()

	// Run tests
	code := m.Run()

	// Cleanup
	os.Exit(code)
}

func TestLogin(t *testing.T) {
	// Setup Echo
	e := echo.New()
	reqBody := `{"username":"testuser","password":"testpassword"}`
	req := httptest.NewRequest(http.MethodPost, "/login", bytes.NewReader([]byte(reqBody)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/login")
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Sprintf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	// Mock database response
	rows := sqlmock.NewRows([]string{"id", "username", "password", "email", "firstname", "lastname"}).
		AddRow(1, "testuser", "$2a$10$WzYXq0n1d9Rm9gUUVdnbY.", "testuser@example.com", "Test", "User")
	mock.ExpectQuery("SELECT id, username, password, email, firstname, lastname FROM users WHERE username = ?").
		WithArgs("testuser").WillReturnRows(rows)

	// Execute handler
	if assert.NoError(t, login(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		var response map[string]interface{}
		err := json.Unmarshal(rec.Body.Bytes(), &response)
		if assert.NoError(t, err) {
			assert.Contains(t, response, "token")
		}
	}
}

func TestRegister(t *testing.T) {
	// Setup Echo
	e := echo.New()
	reqBody := `{"username":"newuser","password":"newpassword","email":"newuser@example.com","firstname":"New","lastname":"User"}`
	req := httptest.NewRequest(http.MethodPost, "/register", bytes.NewReader([]byte(reqBody)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/register")
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Sprintf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	// Mock database response
	mock.ExpectExec("INSERT INTO users").
		WithArgs("newuser", sqlmock.AnyArg(), "newuser@example.com", "New", "User").
		WillReturnResult(sqlmock.NewResult(1, 1))

	// Execute handler
	if assert.NoError(t, register(c)) {
		fmt.Println("code >>", rec.Code)
		assert.Equal(t, http.StatusCreated, rec.Code)
		var response map[string]string
		err := json.Unmarshal(rec.Body.Bytes(), &response)
		if assert.NoError(t, err) {
			assert.Equal(t, "User registered successfully", response["message"])
		}
	}
}

// Helper functions
func login(c echo.Context) error {
	var credentials struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	if err := c.Bind(&credentials); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "Invalid request payload"})
	}

	rows, err := mockDB.Query("SELECT id, username, password, email, firstname, lastname FROM users WHERE username = ?", credentials.Username)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Database error"})
	}
	defer rows.Close()

	var user User
	for rows.Next() {
		err = rows.Scan(&user.ID, &user.Username, &user.Password, &user.Email, &user.FirstName, &user.LastName)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Database error"})
		}
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(credentials.Password)); err != nil {
		return c.JSON(http.StatusUnauthorized, map[string]string{"error": "Invalid username or password"})
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username":  user.Username,
		"email":     user.Email,
		"firstname": user.FirstName,
		"lastname":  user.LastName,
		"exp":       time.Now().Add(time.Hour * 72).Unix(),
	})

	tokenString, err := token.SignedString([]byte(jwtSecretKey))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to generate token"})
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"token":     tokenString,
		"username":  user.Username,
		"email":     user.Email,
		"firstname": user.FirstName,
		"lastname":  user.LastName,
	})
}

func register(c echo.Context) error {
	var user User
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "Invalid request payload"})
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to hash password"})
	}
	user.Password = string(hashedPassword)

	query := "INSERT INTO users (username, password, email, firstname, lastname) VALUES (?, ?, ?, ?, ?)"
	_, err = mockDB.Exec(query, user.Username, user.Password, user.Email, user.FirstName, user.LastName)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "Failed to register user"})
	}

	return c.JSON(http.StatusCreated, map[string]string{"message": "User registered successfully"})
}
